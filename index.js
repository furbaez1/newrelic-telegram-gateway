const express = require("express")
const request = require("request")

const app = express()
const port = 3000

app.use(express.json())

//handle a post to the default route.
//send New Relic alert JSON payload here and make sure the necessary Telegram fields are present, then build the final endpoint URL.
//TODO: secure this! send api key/password in req
app.post("/", (req, res) => {
    const telegramBotAPIKey = req.body.telegramBotAPIKey
    const telegramChatID = req.body.telegramChatID
    const telegramMessage = `Critical priority issue is: ${req.body.state} \n ${req.body.title} \n Policy: ${req.body.alertPolicyNames} \n Conditions: ${req.body.alertConditionNames} \n Incident URL: [Link](${req.body.issueUrl})`
    const telegramEndpoint = `https://api.telegram.org/bot${telegramBotAPIKey}/sendMessage?chat_id=${telegramChatID}&text=${telegramMessage}&parse_mode=markdown`
    res.end()

    //post data to Telegram bot
    request(telegramEndpoint, (error, response) => {
        console.log(response.body)
    })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
